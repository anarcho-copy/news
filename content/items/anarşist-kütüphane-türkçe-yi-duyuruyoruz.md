+++
date = 2020-05-05T08:00:00Z
itemurl = "https://tr.anarchistlibraries.net/special/index"
sites = "tr.anarchistlibraries.net"
tags = ["bildiri"]
title = "Anarşist Kütüphane Türkçe'yi Duyuruyoruz!"

+++
Anarşist Kütüphane Türkçe Yayınlandı! [https://tr.anarchistlibraries.net](https://tr.anarchistlibraries.net "https://tr.anarchistlibraries.net")

Anarşist Kütüphane Türkçe, Anarchist Libraries(Anarşist Kütüphaneler) projesinin bir parçasıdır.

[https://www.anarchistlibraries.net/libraries](https://tr.anarchistlibraries.net "https://tr.anarchistlibraries.net")