+++
date = 2020-11-17T21:00:00Z
itemurl = "https://twitter.com/anarcho_copy/status/1329012684646469632"
sites = "twitter.com"
tags = ["bildiri"]
title = "8 yıl aradan sonra anarşist hacktivist Jeremy Hammond artık özgür! Kendisini selamlıyoruz."

+++
8 yıl aradan sonra anarşist hacktivist Jeremy Hammond artık özgür! Kendisini selamlıyoruz. --- After 8 years, anarchist hacktivist Jeremy Hammond is now free! We salute him. [#FreedJeremy](https://twitter.com/hashtag/FreedJeremy?src=hashtag_click)