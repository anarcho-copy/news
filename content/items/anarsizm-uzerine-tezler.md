+++
date = 2021-03-11T21:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/anarsizm-uzerine-tezler/"
sites = "haber.anarcho-copy.org"
tags = ["pdf"]
title = "Paul Feyerabend'in yazdığı \"Anarşizm Üzerine Tezler\", internet ortamına aktarıldı."

+++
Paul Feyerabend'in yazdığı "Anarşizm Üzerine Tezler", internet ortamına aktarıldı.

* PDF dosyası: [https://anarcho-copy.org/copy/anarsizm-uzerine-tezler](https://anarcho-copy.org/copy/anarsizm-uzerine-tezler "https://anarcho-copy.org/copy/anarsizm-uzerine-tezler") (seçilebilir metin)
* TIFF dosyası: [https://archive.org/download/anarsizm-uzerine-tezler/anarsizm-uzerine-tezler.tif](https://archive.org/download/anarsizm-uzerine-tezler/anarsizm-uzerine-tezler.tif "https://archive.org/download/anarsizm-uzerine-tezler/anarsizm-uzerine-tezler.tif") (taranmış orjinal belge, arşiv formatı)
* Internet Archive: [https://archive.org/details/anarsizm-uzerine-tezler](https://archive.org/details/anarsizm-uzerine-tezler/ "https://archive.org/details/anarsizm-uzerine-tezler/")

![](/uploads/filename:20210318/ewttejhxeam8rv9.jpeg)