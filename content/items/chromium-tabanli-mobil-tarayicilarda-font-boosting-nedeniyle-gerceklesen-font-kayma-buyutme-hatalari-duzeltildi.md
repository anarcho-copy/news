+++
date = 2021-02-26T00:00:00Z
itemurl = "https://anarcho-copy.org"
sites = "anarcho-copy.org"
tags = ["bildiri"]
title = "Chromium tabanlı mobil tarayıcılarda font boosting nedeniyle gerçekleşen font kayma/büyütme hataları düzeltildi."

+++
Chromium tabanlı mobil tarayıcılarda [font boosting](https://bugs.chromium.org/p/chromium/issues/detail?id=138257) nedeniyle gerçekleşen font kayma/büyütme hataları düzeltildi.

 

![](/uploads/filename:20210226/font-ss.jpeg)