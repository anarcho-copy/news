+++
date = 2021-03-27T21:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/ozgur-bir-toplumda-bilim/"
sites = "haber.anarcho-copy.org"
tags = ["pdf"]
title = "Paul Feyerabend'in yazdığı \"Özgür Bir Toplumda Bilim\", internet ortamına aktarıldı."

+++
Paul Feyerabend'in yazdığı "Özgür Bir Toplumda Bilim", internet ortamına aktarıldı.

* PDF dosyası: [https://anarcho-copy.org/copy/ozgur-bir-toplumda-bilim](https://anarcho-copy.org/copy/ozgur-bir-toplumda-bilim "https://anarcho-copy.org/copy/ozgur-bir-toplumda-bilim") (seçilebilir metin)
* TIFF dosyası: [https://archive.org/download/ozgur-bir-toplumda-bilim/ozgur-bir-toplumda-bilim.tif](https://archive.org/download/ozgur-bir-toplumda-bilim/ozgur-bir-toplumda-bilim.tif "https://archive.org/download/ozgur-bir-toplumda-bilim/ozgur-bir-toplumda-bilim.tif") (taranmış orjinal belge, arşiv formatı)
* Internet Archive: [https://archive.org/details/ozgur-bir-toplumda-bilim](https://archive.org/details/ozgur-bir-toplumda-bilim "https://archive.org/details/ozgur-bir-toplumda-bilim")

![](/uploads/filename:20210331/exlnseuxiayzw17.jpeg)