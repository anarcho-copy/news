+++
date = 2021-04-02T13:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/federasyon/"
sites = "haber.anarcho-copy.org"
tags = ["bildiri"]
title = "Devrimci Anarşist Federasyon kuruldu!"

+++
Devrimci Anarşist Federasyon kuruldu! 

Deklarasyon metni, "Federasyona Çağırıyoruz!": 

* [https://tr.anarchistlibraries.net/library/federasyon](https://tr.anarchistlibraries.net/library/federasyon "https://tr.anarchistlibraries.net/library/federasyon") 
* [https://anarsistfederasyon.org/federasyona-cagiriyoruz](https://anarsistfederasyon.org/federasyona-cagiriyoruz "https://anarsistfederasyon.org/federasyona-cagiriyoruz")
* [https://anarcho-copy.org/federasyon](https://anarcho-copy.org/federasyon "https://anarcho-copy.org/federasyon")

![](/uploads/filename:20210402/federasyon.png)