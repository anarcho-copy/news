+++
date = 2019-11-28T13:20:00Z
itemurl = "https://haber.anarcho-copy.org/items/daha-%C3%B6nce-tarad%C4%B1%C4%9F%C4%B1m%C4%B1z-veya-internette-bulunan-9-kitap-aranabilir-pdf-haline-getirilerek-redakte-edildi-art%C4%B1k-metinler-se%C3%A7ilebilir-olacak/"
sites = "haber.anarcho-copy.org"
tags = ["bildiri"]
title = "daha önce taradığımız veya internette bulunan 9 kitap aranabilir pdf haline getirilerek redakte edildi.  Artık metinler seçilebilir olacak."

+++
Redakte edilen kitaplar:

1. [https://anarcho-copy.org/copy/7-haziran-genel-secimlerine-dair-demokrasi-ve-mesruluk/](https://anarcho-copy.org/copy/7-haziran-genel-secimlerine-dair-demokrasi-ve-mesruluk/ "https://anarcho-copy.org/copy/7-haziran-genel-secimlerine-dair-demokrasi-ve-mesruluk/")
2. [https://anarcho-copy.org/copy/alt-kultur-tarzin-anlami/](https://anarcho-copy.org/copy/alt-kultur-tarzin-anlami/ "https://anarcho-copy.org/copy/alt-kultur-tarzin-anlami/")
3. [https://anarcho-copy.org/copy/an-anarchist-criticism-to-occupy-as-an-activity-of-99/](https://anarcho-copy.org/copy/an-anarchist-criticism-to-occupy-as-an-activity-of-99/ "https://anarcho-copy.org/copy/an-anarchist-criticism-to-occupy-as-an-activity-of-99/")
4. [https://anarcho-copy.org/copy/anarsist/](https://anarcho-copy.org/copy/anarsist/ "https://anarcho-copy.org/copy/anarsist/")
5. [https://anarcho-copy.org/copy/anarsist-bir-film-teorisine-dogru/](https://anarcho-copy.org/copy/anarsist-bir-film-teorisine-dogru/ "https://anarcho-copy.org/copy/anarsist-bir-film-teorisine-dogru/")
6. [https://anarcho-copy.org/copy/anarsizmin-abcsi/](https://anarcho-copy.org/copy/anarsizmin-abcsi/ "https://anarcho-copy.org/copy/anarsizmin-abcsi/")
7. [https://anarcho-copy.org/copy/anarsizmin-tarihi/](https://anarcho-copy.org/copy/anarsizmin-tarihi/ "https://anarcho-copy.org/copy/anarsizmin-tarihi/")
8. [https://anarcho-copy.org/copy/anarsizm-nedir/](https://anarcho-copy.org/copy/anarsizm-nedir/ "https://anarcho-copy.org/copy/anarsizm-nedir/")
9. [https://anarcho-copy.org/copy/bagimsiz-ve-savasimci-bir-sendika-sendikalistler/](https://anarcho-copy.org/copy/bagimsiz-ve-savasimci-bir-sendika-sendikalistler/ "https://anarcho-copy.org/copy/bagimsiz-ve-savasimci-bir-sendika-sendikalistler/")