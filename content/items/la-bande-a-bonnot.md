+++
date = 2021-02-27T15:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/la-bande-a-bonnot"
sites = "archive.org"
tags = ["film"]
title = "Yeni bir çeviri, \"Bonnot Çetesi (La Bande A Bonnot)\" filmi:"

+++
Yeni bir çeviri, "Bonnot Çetesi (La Bande A Bonnot)" filmi.

İzlemek için: [Bonnot Çetesi (La Bande A Bonnot) Türkçe Altyazılı - archive.org](https://archive.org/details/bonnotcetesi "Bonnot Çetesi (La Bande A Bonnot) Türkçe Altyazılı - archive.org")

> Aynı zamanda filmin adı olan Bonnot Çetesi, 1911'de Fransa'da bir araya gelmiş, Eylemle Propaganda Kuşağı'nın cesur temsilcisi anarşistlerin oluşturduğu, dünyanın ilk arabalı soygunu gibi işlere imza atan, devleti döneminde tir tir titreten bir gruptu. Çetenin hedefi genellikle bankalar ve burjuvaziydi.

&nbsp;

 ![](/uploads/filename:20210227/60989911.webp)