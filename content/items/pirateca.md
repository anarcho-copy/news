+++
date = 2021-03-02T00:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/pirateca/"
sites = "haber.anarcho-copy.org"
tags = ["bildiri"]
title = "Latin Amerika'da filizlenen anarşist bir korsan kolektif: \"La Pirateca\""

+++
Latin Amerika'da filizlenen anarşist bir korsan kolektif: "La Pirateca" [@la_pirateca](https://twitter.com/la_pirateca) | [pirateca.com](https://pirateca.com)

"Tüm kültür ve tüm bilgiler ortak bir deneyimden doğar." 

...


"Bizi ilgilendiren her şey, bizim için önemlidir ve bize aittir. Her sanat eseri, her şiir, her felsefe, her müzik ortak bir deneyimden, insan deneyiminden (ki bu her zaman ortaktır) doğar ve bu nedenle, bir bireyden çok, tüm bilgi ve kültür insanlığa aittir."

&nbsp;

Proudhon haklı olarak: "mülkiyet hırsızlıktır" demişti. Şimdi şunu söylüyoruz: fikri mülkiyetlerin hepsi birer fikri hırsızlıktır. Bir bireyin bilgi birikimi diğer birçok bireyin birikimi değilse nedir?

&nbsp;

Tek bir özne nasıl bir kavramın, bir sesin, bir fikrin, bir dizenin, bir yaşam tarzının mutlak sahibi olabilir? Kültürel bir nesneyi sahiplenmeye ve ondan yararlanmaya yönelik herhangi bir girişimin unutulma, sahipsizleştirme, hırsızlık ve özelleştirme olduğuna inanıyoruz.

&nbsp;

Dolayısıyla, fikri mülkiyet ve telif hakkı sistemi olan bu kalıcı ve sistematik mülksüzleştirme ve unutkanlığa karşı direnmekten başka çaremiz yok. Kamulaştırarak, özgürleştirerek, "çalalarak", hatırlatarak korsanca direniyor; bilgiyi gerçek sahibine, hepimize armağan ediyoruz.

&nbsp;

![](/uploads/filename:20210304/pirateca.jpeg)