+++
date = 2020-06-26T21:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/27-haziran-2020-duyuru/"
sites = "haber.anarcho-copy.org"
tags = ["bildiri"]
title = "27 Haziran 2020 - DUYURU"

+++
Merhaba,

Satın alınan PDF belgelerinde dijital telif hakkı koruması, DRM, no-copying vs. ile ilgili teknikleri incelemeye ve buna karşı alınacak önlemler için araştırma-geliştirme çalışmasına başladık.  
  
Sizden ricamız son zamanlarda satın aldığınız Dijital Kitabı; meta/xmp data, embedded data incelemesi yapabilmemiz için dosyayı bize göndermeniz.  
Dijital haklar yönetimi PDF belgelerinde benzersiz ID belirleyip herhangi bir kopyalama durumunda sizi ifşa etmek için kullandıklarından dolayı verdiğiniz dosya orjinal haliyle kesinlikle web ortamında paylaşılmayacaktır.  
  
  
Dosyayı göndermek için mail adresimizi kullanabilirsiniz anarchocopy@protonmail.com, dosyayı şifreleyip göndermek isterseniz public-key: [https://anarcho-copy.org/etc/public-key.gpg](https://anarcho-copy.org/etc/public-key.gpg "https://anarcho-copy.org/etc/public-key.gpg")