+++
date = 2021-06-11T21:00:00Z
itemurl = "http://edu.anarcho-copy.org"
sites = "edu.anarcho-copy.org"
tags = ["bildiri"]
title = "EDU.Anarcho-Copy.org geçici süreliğine servis dışı."

+++
Anarcho-Copy geçtiğimiz haftalarda yaşanan servis sağlayıcı sorunlarından dolayı 4 gün boyunca servis dışı kaldı, edu.anarcho-copy.org ise tamamen servis dışı. Bu tarz sorunların çözülmesine kadar anlayışla karşılanmasını bekliyoruz.