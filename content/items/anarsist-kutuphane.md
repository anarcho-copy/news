+++
date = 2021-02-07T00:00:00Z
itemurl = "https://twitter.com/anarsistyayim/status/1358419478841819138"
sites = "twitter.com"
tags = ["bildiri"]
title = "Anarşist Kütüphane'nin Twitter hesabı ve Telegram kanalı oluşturuldu."

+++
* Web: [tr.anarchistlibraries.net](http://tr.anarchistlibraries.net)
* Tor: [anarsizmhxyuq7nfuw2hirvflh2ly2p3ddczsnmxmbsn73rgiyytpyqd.onion](http://anarsizmhxyuq7nfuw2hirvflh2ly2p3ddczsnmxmbsn73rgiyytpyqd.onion)
* Twitter: [@anarsistyayim](https://twitter.com/anarsistyayim)
* Telegram: [@anarsistkutuphane](http://t.me/anarsistkutuphane)