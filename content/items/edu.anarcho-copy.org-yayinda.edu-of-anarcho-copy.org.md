+++
date = 2020-02-18T21:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/edu.anarcho-copy.org-yayinda.edu-of-anarcho-copy.org/"
sites = "edu.anarcho-copy.org"
tags = ["bildiri"]
title = "edu.anarcho-copy.org , Yayında. (EDU of anarcho-copy.org)"

+++
[edu.anarcho-copy.org](http://edu.anarcho-copy.org/) nedir? Programlama dili, Unix ve benzerleri İşletim Sistemleri, ağ ve güvenliğine dair dokümanların bulundugu özgür öğrenim kaynak deposudur.
