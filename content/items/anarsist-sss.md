+++
date = 2021-02-11T00:00:00Z
itemurl = "https://tr.anarchistlibraries.net/library/anarsist-sss-2007"
sites = "tr.anarchistlibraries.net"
tags = ["pdf"]
title = "Anarşist Bakış'ın çevirdiği Anarşist SSS 11.2 (2007), kütüphane ortamına aktarıldı."

+++
Bu Anarşist SSS versiyon 11.2'ye (2007) dayanmakta olup çevirisi Anarşist Bakış tarafından yapılmıştır. An Anarchist FAQ'ın son sürümü 15.3 (2019) olup İngilizce canlı sürümü [ananarchistfaq.anarcho-copy.org](https://ananarchistfaq.anarcho-copy.org) adresinde kullanılabilir.

[Anarşist Sıkça Sorulan Sorular - Versiyon 11.2 ](https://tr.anarchistlibraries.net/library/anarsist-sss-2007)