+++
date = 2019-12-17T08:59:00Z
itemurl = "https://twitter.com/anarcho_copy/status/1206866094037913600?s=19"
sites = "twitter.com/anarcho_copy"
tags = ["bildiri"]
title = "İnternetin bir politika alanı olmasından daha doğal bir şey olamaz, çünkü orası toplumsal bir alan, bir topluluklar mekanı."

+++
İnternetin bir politika alanı olmasından daha doğal bir şey olamaz, çünkü orası toplumsal bir alan, bir topluluklar mekanı. Siber kültürün isimlerinden Bruce Sterling'in dediği gibi: “Topluluk (Community) ve iletişim (Communication) sözcükleri aynı köke sahiptir. Bir iletişim ağı kurduğunuz her yerde bir topluluk da kurarsınız ve ne zaman bu ağı yıkarsanız -ele geçirirseniz, yasadışı ilan ederseniz, çökertirseniz ya da erişilemeyecek kadar pahalı kılarsanız-, topluluğu da incitmiş olursunuz."