+++
date = 2020-03-20T21:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/covid-19-onleme-ve-tedavi-el-kitabi-pdf/"
sites = "anarcho-copy.org"
tags = ["pdf"]
title = "Covid 19 Önleme ve Tedavi El Kitabı PDF"

+++
Covid 19 Önleme ve Tedavi El Kitabı PDF - Zhejiang Üniversitesi Tıp Fakültesi   
  
Çin'de Covid-19 ile mücadele eden doktorlar, tedavi sürecinde bir el kitabı hazırladılar. Anonim kaynaklar bu kitabı çevirdi. Detaylı bir rehber halindedir.

[https://anarcho-copy.org/copy/covid-19-onleme-ve-tedavi-el-kitabi/](https://anarcho-copy.org/copy/covid-19-onleme-ve-tedavi-el-kitabi/ "https://anarcho-copy.org/copy/covid-19-onleme-ve-tedavi-el-kitabi/")