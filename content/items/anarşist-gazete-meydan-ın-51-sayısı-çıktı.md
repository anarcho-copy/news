+++
date = 2019-11-01T03:54:00Z
itemurl = "https://meydan.org/2019/11/01/anarsist-gazete-meydanin-51-sayisi-cikti/"
sites = "meydan.org"
tags = ["bildiri"]
title = "Anarşist Gazete Meydan’ın 51. Sayısı Çıktı"

+++
Aylık olarak yayınlanan anarşist gazete Meydan, 51. sayısını çıkardı. Bu ay “Popülizmi İzleme, Devrimi Eyle” manşetiyle çıkan Meydan’ın 51. sayısının konseptindeki konuyu da son yılların çok tartışılan kavramı olan “popülizm” oluşturdu. Popülizm başlığı altında, _“Popülokrasi”_ , _“İklim Popülizmi – Beş Başlıkta İklim Grevi’nin Değerlendirmesi”_ ve Gökhan Soysal imzalı _“Popülizm Nedir, Ne Değildir? ”_ yazıları yer aldı.

Meydan Gazetesi’nin 51. sayısının diğer yazıları ve konu başlıkları ise şu şekilde oluştu:

– Seçimden Sonra Başkanların İlk Hizmeti: İşten Atma

– Kursu Yurdu Vakfıyla, Müridi Hocası Şeyhiyle; Dinsel Şiddet – Hüseyin Civan

– Örgütlü Bir Halkı Hiçbir Felaket Yenemez – Gizem Şahin

– Sahadan Masaya Savaş – Emrah Tekin

– Dünya İsyanda – Merve Arkun

– Lübnan’da Halk Düzenin Değişmesini İstiyor – Lea Khalil

– Hayırseverliğin Sektör Hali: Üçüncü Sektör Vakfı – Selahattin Hantal

– Kadınlar Kazanacak Devlet Kaybedecek – Şeyma Çopur

– Anarşist Kadınlar’dan 25 Kasım Çağrısı

– Hannah’lar: Kötülük Üzerine – Mercan Doğan

– Gençlik Anarşizmde Örgütleniyor

– Kılavuz Kavramlar (1): Otorite

– Anarşist Bir Demiryolcunun Hikayesi: Bizim Ali

– Anarşistlerin Teori ve Pratik Tartışmaları (4): Evrimci Anarşizm ve Devrimci Anarşizm

– Enternasyonelin Son, Geleneğin İlk Mohikanları: Jura Federasyonu – İlyas Seyrek

– Tarihin Derinliklerine Kara Bir El: Mano Negra – Furkan Çelik

– Nietzsche Üzerine Bir Röportaj

– Yalınayak: Trans Tutsak Buse’den Mektup

– Robotikleştiriliyor muyuz Kodlanıyor muyuz? – Özgür Oktay

– Anonsofobi – İrem Gülser

– İspanya’da Özgür Bir Köy: Fraguas – Jorge Martin