+++
date = 2021-01-15T00:00:00Z
itemurl = "https://basibozukyayin.com"
sites = "basibozukyayin.com"
tags = ["bildiri"]
title = "\"Bilgi özel mülkiyet değildir, paylaşın.\" mottosuyla yola çıkan yeni bir kolektif yayın: \"Başıbozuk Yayın\". "

+++
"Bilgi özel mülkiyet değildir, paylaşın." mottosuyla yola çıkan yeni bir kolektif yayın: "Başıbozuk Yayın". Yayımladıkları kitapların erişimlerini bir kısıtlamaya tabi tutmadan özgür erişim felsefesiyle sağlamaktalar.

Web adresleri: [basibozukyayin.com](http://basibozukyayin.com "http://basibozukyayin.com") 
Twitter: [@basibozukyayin](https://twitter.com/basibozukyayin)