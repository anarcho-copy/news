+++
date = 2020-12-22T21:00:00Z
itemurl = "https://twitter.com/anarcho_copy/status/1341686790944665600"
sites = "colectivodisonancia.net"
tags = ["bildiri"]
title = "\"Bilgiyi kopyalamak ve kollektifleştirmek, sermayenin bilgiye dayattığı sınırlarla yüzleşmek için özerklik sağlar.\""

+++
"Bilgiyi kopyalamak ve kollektifleştirmek, sermayenin bilgiye dayattığı sınırlarla yüzleşmek için özerklik sağlar."

"Teknolojiye erişim, nasıl üretileceğine veya etkilerinin ne olduğuna karar verme yeteneğine sahip olmak anlamına gelmez. Kapitalizmde teknolojiye tüketiciler veya kullanıcılar olarak erişiriz, ancak nadiren kullandığımız medyayı şekillendiren sosyal ilişkilerin üreticisi oluruz. 

Teknolojik Özerklik, toplulukların teknolojiyi kontrol ederek onu kolektif ihtiyaçların ve çıkarların hizmetine sunma ufkudur. Bu özerklik uğruna mücadele, bugün kapitalist ve teknokratik tahakkümün ellerinde, teknolojik araçların kolektifleşmesi ve özgürleşmesi için verilen bir mücadeledir."

[colectivodisonancia.net](https://colectivodisonancia.net/ "colectivodisonancia.net")   [@cdisonancia](https://twitter.com/cdisonancia)