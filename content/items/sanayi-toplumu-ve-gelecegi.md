+++
date = 2021-02-20T00:00:00Z
itemurl = "https://tr.anarchistlibraries.net/library/sanayi-toplumu-ve-gelecegi"
sites = "tr.anarchistlibraries.net"
tags = ["pdf"]
title = "Kaos Yayınları'nın çevirdiği Theodore Kaczynski'nin yazdığı \"Sanayi Toplumu ve Geleceği\", kütüphane ortamına aktarıldı."

+++
* [anarcho-copy.org/copy/sanayi-toplumu-ve-gelecegi/](https://anarcho-copy.org/copy/sanayi-toplumu-ve-gelecegi/)
* [tr.anarchistlibraries.net/library/sanayi-toplumu-ve-gelecegi](https://tr.anarchistlibraries.net/library/sanayi-toplumu-ve-gelecegi)