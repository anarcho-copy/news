+++
date = 2020-07-07T05:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/status/"
sites = "haber.anarcho-copy.org"
tags = ["bildiri"]
title = "(STATUS) Sunucu Bakım İşlerine Başladık. Birçok Servis, Servis Dışıdır. Detaylı bilgi için tıkla. | We Started Server Maintenance Works. Many Services are Out of Service."

+++
Geçici veya tam süreliğine servis dışı olanlar:

* [link.anarcho-copy.org](https://link.anarcho-copy.org/) (tam)
* [video.anarcho-copy.org](https://video.anarcho-copy.org/) (tam)
* [search.anarcho-copy.org](https://search.anarcho-copy.org/) (tam)
* [pad.anarcho-copy.org](https://pad.anarcho-copy.org/) (tam)
* [chat.anarcho-copy.org](https://chat.anarcho-copy.org/) (tam)
* [upload.anarcho-copy.org](https://upload.anarcho-copy.org/) (tam)
* [yayin.anarcho-copy.org](https://yayin.anarcho-copy.org "yayin.anarcho-copy.org") (tam)

Çalışan servisler/siteler:

* [anarcho-copy.org](https://anarcho-copy.org/)
* [edu.anarcho-copy.org](https://edu.anarcho-copy.org/ "https://edu.anarcho-copy.org/")
* [build.anarcho-copy.org](https://build.anarcho-copy.org/ "build.anarcho-copy.org/")
* [git.anarcho-copy.org](https://git.anarcho-copy.org/ "https://git.anarcho-copy.org/")
* [haber.anarcho-copy.org](https://haber.anarcho-copy.org)
* [alpersapan.anarcho-copy.org](https://alpersapan.anarcho-copy.org/) 
* [ananarchistfaq.anarcho-copy.org](https://ananarchistfaq.anarcho-copy.org/) 

```
1599418395
Sun 06 Sep 2020 06:53:58 PM UTC
```
