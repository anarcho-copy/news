+++
date = 2021-02-25T08:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/tor/"
sites = "anarcopym4ckplfg3uljfj37y27ko3vrnrp43msyu5k5rp3h46wtf7yd.onion"
tags = ["bildiri"]
title = "Anarcho-Copy'in tüm servisleri artık tor ağını destekliyor!"

+++
* [anarcopym4ckplfg3uljfj37y27ko3vrnrp43msyu5k5rp3h46wtf7yd.onion](http://anarcopym4ckplfg3uljfj37y27ko3vrnrp43msyu5k5rp3h46wtf7yd.onion) | anarcho-copy.org
* [educate6mw6luxyre24uq3ebyfmwguhpurx7ann635llidinfvzmi3yd.onion](http://educate6mw6luxyre24uq3ebyfmwguhpurx7ann635llidinfvzmi3yd.onion) | edu.anarcho-copy.org
* [build32swgic4aioq7cm3bfz6qemtyxyftb7o7kzheh7x6wa3lnkwxid.onion](http://build32swgic4aioq7cm3bfz6qemtyxyftb7o7kzheh7x6wa3lnkwxid.onion) | build.anarcho-copy.org
* [gogs4qoamj7tldcvlgpht2hxaoug3xui44tclj34koqaowu6lyb2payd.onion](http://gogs4qoamj7tldcvlgpht2hxaoug3xui44tclj34koqaowu6lyb2payd.onion) | git.anarcho-copy.org
* [haberlkqrr4fmwttj3d74qrcblut6vxb6i4yunhg7gwljconppinxhad.onion](http://haberlkqrr4fmwttj3d74qrcblut6vxb6i4yunhg7gwljconppinxhad.onion) | haber.anarcho-copy.org
* [alperqywbivqpu6vu7yxlfnw3milysg2aiicm5vkz6x3bhsfvfpzf7yd.onion](http://alperqywbivqpu6vu7yxlfnw3milysg2aiicm5vkz6x3bhsfvfpzf7yd.onion) | alpersapan.anarcho-copy.org
* [libfaqf556m6sqcme7kseel4zehmie6pkpu47bzbqzzaofn5xattfxad.onion/](http://libfaqf556m6sqcme7kseel4zehmie6pkpu47bzbqzzaofn5xattfxad.onion) | ananarchistfaq.anarcho-copy.org
* [anarsistxbwzyu3ca2xizuou6ddmdninfq5annm3hz4ghn3eyciyw3qd.onion](http://anarsistxbwzyu3ca2xizuou6ddmdninfq5annm3hz4ghn3eyciyw3qd.onion) | anarsistsss.anarcho-copy.org
* [mirrortndojq27wk4gk3nboxstdu2wupen6mgowybuyexi4riv7re6id.onion](http://mirrortndojq27wk4gk3nboxstdu2wupen6mgowybuyexi4riv7re6id.onion) | mirror.anarcho-copy.org
* [indir3sj4ufuapyg7v6ztc3yptm77c7yy6sjavkaxnjxvla3djqfrmyd.onion](http://indir3sj4ufuapyg7v6ztc3yptm77c7yy6sjavkaxnjxvla3djqfrmyd.onion/) | indir.anarcho-copy.org
* [statusfpqxa6prfesv6gpmz74uteuagk3jltcjlgyeeeop6p4zsy5kyd.onion](http://statusfpqxa6prfesv6gpmz74uteuagk3jltcjlgyeeeop6p4zsy5kyd.onion) | status.noblogs.org

Tor yönlendirmesi için tor adlı subdomani kullanabilirsin:

**tor.anarcho-copy.org/copy/tanri-ve-devlet ==> anarcopy...onion/copy/tanri-ve-devlet**

&nbsp;

veya

&nbsp;

**Tor Browser'ın sağ üst köşesindeki mor ikona tıklayabilirsin:**

&nbsp;

![](/uploads/filename:20210226/ss.png)