+++
date = 2020-05-18T21:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/anarcho-copy-in-alt-yapisi-temasi-ve-generatoru-tekrardan-insa-edilmekte/"
sites = "haber.anarcho-copy.org"
tags = ["bildiri"]
title = "anarcho-copy'in alt yapısı, teması ve generatörü tekrardan inşa edilmekte:"

+++
anarcho-copy'in alt yapısı, teması ve generatörü tekrardan inşa edilmekte:

inşa edilen projeyi geliştirme, önerme ve katkı için açtık.

Proje Reposu: [https://git.anarcho-copy.org/www.anarcho-copy.org/hidden_site_generator](https://git.anarcho-copy.org/www.anarcho-copy.org/hidden_site_generator "https://git.anarcho-copy.org/www.anarcho-copy.org/hidden_site_generator")

GitHub: [https://github.com/anarcho-copy/hidden_site_generator](https://github.com/anarcho-copy/hidden_site_generator "https://github.com/anarcho-copy/hidden_site_generator")

Canlı Sürüm: [https://build.anarcho-copy.org/](https://build.anarcho-copy.org/ "https://build.anarcho-copy.org/")