+++
date = 2021-03-14T21:00:00Z
itemurl = "https://haber.anarcho-copy.org/items/cinsel-cesitlilik/"
sites = "haber.anarcho-copy.org"
tags = ["pdf"]
title = "Vanessa Baird'in yazdığı \"Cinsel Çeşitlilik: Yönelimler, Politikalar, Haklar ve İhlaller\", internet ortamına aktarıldı. 🌈"

+++
Vanessa Baird'in yazdığı "Cinsel Çeşitlilik: Yönelimler, Politikalar, Haklar ve İhlaller", internet ortamına aktarıldı. 🌈

* PDF dosyası: [https://anarcho-copy.org/copy/cinsel-cesitlilik](https://anarcho-copy.org/copy/cinsel-cesitlilik "https://anarcho-copy.org/copy/cinsel-cesitlilik") (seçilebilir metin)
* TIFF dosyası: [https://archive.org/download/cinsel-cesitlilik/cinsel-cesitlilik.tif](https://archive.org/download/cinsel-cesitlilik/cinsel-cesitlilik.tif "https://archive.org/download/cinsel-cesitlilik/cinsel-cesitlilik.tif") (taranmış orjinal belge, arşiv formatı)
* Internet Archive: [https://archive.org/details/cinsel-cesitlilik](https://archive.org/details/cinsel-cesitlilik "https://archive.org/details/cinsel-cesitlilik")

![](/uploads/filename:20210318/ewibi9kwgam2qe5.jpeg)